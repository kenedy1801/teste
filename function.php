<?php
function get_pdo(){
  $config = require 'config.php';
  $pdo = new PDO($config['database_dsn'], $config['database_user'], $config['database_pass']);
  return $pdo;
}

function save_survey($survey, $options){
  $options = filter_array($options);
  $options = json_encode($options);
  $pdo = get_pdo();
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $query =  'INSERT INTO surveys (survey,options) VALUES (:survey, :options)';
  $stmt = $pdo->prepare($query);
  $stmt->bindParam(':survey', $survey);
  $stmt->bindParam(':options', $options);
  $stmt->execute();
}

function get_surveys(){
  $pdo = get_pdo();
  $query = 'SELECT * FROM surveys';
  $stmt = $pdo->prepare($query);
  $stmt->execute();
  $surveys = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $all_surveys = array();
  foreach ($surveys as $key => $survey){
    $all_surveys[$key]['survey'] = $survey['survey'];
    $all_surveys[$key]['options'] = json_decode($survey['options']);
  }
  return $all_surveys;
}
function filter_array($array){
  return array_filter($array, function($value) { return $value !== ''; });
}