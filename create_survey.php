<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="survey.js"></script>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <h1>Create your survey</h1>
  <form method="post" enctype="multipart/form-data" action="index.php">
    <div class="form-group header">
      <label for="survey" class="control-label">Survey</label>
      <input type="text" name="survey" id="survey" class="form-control" size="55">
    </div>
    <button type="button" class="btn-options">Add Option</button>
    <button type="submit" class="btn-submit">Submit</button>
  </form>
</body>