<?php
require 'function.php';
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method == 'POST') {
  extract($_POST);
  if(isset($survey) && isset($options) ) {
    save_survey($survey, $options);
  }
}
?>
<head>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <form action="create_survey.php">
    <input type="submit" class="btn-home" value="Create a survey!"/>
  </form>
  <form action="show_surveys.php">
    <input type="submit" class="btn-home" value="Show all surveys" />
  </form>
</body>