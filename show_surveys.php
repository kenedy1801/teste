<?php
  require 'function.php';
  $all_survey = get_surveys();
?>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h1>All surveys</h1>
  <form action="index.php">
    <?php foreach ($all_survey as $value) : ?>
      <h2><?php print($value['survey']); ?></h2>
      <select name="options" style='min-width: 70px;'>
       <?php foreach($value['options'] as $key => $option) : ?>
         <option value="<?php print($key); ?>"><?php print($option); ?></option>
       <?php endforeach; ?>
      </select>
    <?php endforeach; ?>
    <br>
    <input type="submit" class="btn-home" value="HOME">
  </form>
</body>